package Ex2;

public class ContaPoupanca extends Conta {

	private double taxaRendimento;
	private double taxaDeposito;

	public ContaPoupanca(double saldo, double taxaRendimento, int numeroConta) {
		super(saldo, numeroConta);
		this.taxaRendimento = taxaRendimento;
	}

	public void atualizaSaldo() {
		this.saldo += this.saldo * taxaRendimento;
	}

	public void atualiza(double taxaSelic) {

		this.saldo += this.saldo * taxaSelic;
	}

	public void deposita(double valor) {
		this.saldo += valor - taxaDeposito;
	}

	public double getTaxaRendimento() {
		return taxaRendimento;
	}

	public void setTaxaRendimento(double taxaRendimento) {
		this.taxaRendimento = taxaRendimento;
	}

	public double getTaxaDeposito() {
		return taxaDeposito;
	}

	public void setTaxaDeposito(double taxaDeposito) {
		this.taxaDeposito = taxaDeposito;
	}

}