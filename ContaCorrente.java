package Ex2;

public class ContaCorrente extends Conta implements Tributavel{

	private double taxaManutencao;
	private double taxaDeposito;
	private double taxaRendimento;

	public double calculaTributos(){
		return this.getSaldo() * 0.01;
	}
	
	public ContaCorrente(double saldo, int numeroConta, double taxaManutencao,
			double taxaDeposito, double taxaRendimento) {
		super(saldo, numeroConta);
		this.taxaManutencao = taxaManutencao;
		this.taxaDeposito = taxaDeposito;
		this.taxaRendimento = taxaRendimento;
	}

	public void atualizaSaldo() {

		this.saldo += this.saldo * taxaRendimento;
	}

	public void atualiza(double taxaSelic) {

		this.saldo += this.saldo * taxaSelic;
	}

	public double Manutencao() {

		return this.saldo * this.taxaManutencao;
	}

	public void deposita(double valor) {
		this.saldo += valor - taxaDeposito;
	}

	public double getTaxaRendimento() {
		return taxaRendimento;
	}

	public void setTaxaRendimento(double taxaRendimento) {
		this.taxaRendimento = taxaRendimento;
	}

	public double getTaxaManutencao() {
		return taxaManutencao;
	}

	public void setTaxaManutencao(double taxaManutencao) {
		this.taxaManutencao = taxaRendimento;
	}

	public double getTaxaDeposito() {
		return taxaDeposito;
	}

	public void setTaxaDeposito(double taxaDeposito) {
		this.taxaDeposito = taxaDeposito;
	}

}