package Ex2;

public abstract class Conta {

	// Atributos
	protected double saldo;
	private int numeroConta;

	void saca(double valor) {
		this.saldo -= valor;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}

	public void printSaldo() {
		System.out.println(this.saldo);
	}

	public abstract void atualiza(double taxaSelic);

	// Construtor
	public Conta(double saldo, int numeroConta) {
		this.saldo = saldo;
		this.numeroConta = numeroConta;
	}

	public Conta() {
	}

}